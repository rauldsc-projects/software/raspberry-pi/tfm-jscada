import time, datetime, os	# Python modules
import ezdb			# Own modules

db = '/home/pi/database/database.db'

# Get the config datasend info from DB
config = dict(ezdb.select_table(db, 'sendinfo'))
hour_to_send, minute_to_send = config['hour'], config['minute']
id, model, key = config['id'], config['model'], config['pass']

# Execute an infinite loop checking the date
while True:

	# Get the actual date and extract the hour and the minute
	actual_date = datetime.datetime.now()
	actual_hour = actual_date.strftime('%H')
	actual_minute = actual_date.strftime('%M')

	# Check the actual hour and minute with the configurated ones
	if actual_date.hour == hour_to_send and actual_date.minute == minute_to_send:

		os.system('sudo php /home/pi/datasend/send.php ' + str(id) + ' ' + model + ' ' + key)
		time.sleep(60)

	# A little stop to check every 10 seconds
	time.sleep(30)
