"""
    ezdb.py - rauldsc94@gmail.com - Raul Dominguez

    ### ONLY NEEDS TO IMPORT THE MODULE ###

    This is a module to use in the easy way a SQLite database.
    The functions are prepare to work with SQLite3 in Python 3.8.

    When imported, you can access the functions as "ezdb.xxx", where "xxx"
    is the call of the function.

    Functions Available:
        -> cpath*

            Check the path given. Usefull only in the module internally by the other functions.

            PARAMS: path (String with the Data Base path).
            RETURNS: True if is a valid path, False if not.

            *internal function, you can use it if you want, but is needed and used for all the
            functions in this module.

        -> create_table

            Create a table in the Data Base with the name and columns given.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name to put to the new table),
                    columns_info (A dictionary with the info about the columns, must be like:

                    ##    columns_info = {
                    ##        0: { "name": "column_name", "type": "TEXT" },
                    ##        1: { "name": "column_name", "type": "INTEGER", "extra": "NULL" }
                    ##        2: { "name": "column_name", "type": "INTEGER" }
                    ##    }

                    where "name" is the name of the column, "type" is the SQLite3 data type of the
                    column, and "extra" is some SQLite3 flag needed for the column like PRIMARY KEY
                    or NULL).
            RETURNS: True if the table was created, False if something went wrong.

        -> delete_table

            Delete a table in the Data Base if exists the table with the name given.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table that have to be deleted).
            RETURNS: True if the table was deleted or if it doesn't exist, False if not.

        -> select_table

            Get a complete table with the name given.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table that have to be collected).
            RETURNS: A list of tuples with the complete table data, False if something went wrong.

        -> select_row

            Get the row with the row ID given from the table with the name given.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table from where collect the row),
                    row_id (The ID of the row to be collected).
            RETURNS: A list with the row, False if something went wrong.

        -> select_column

            Get the column from the table with the names given.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table from where collect the column),
                    column_name (The name of the column to be collected).
            RETURNS: A list with the column, False if something went wrong.

        -> select_value

            Get the value from the table with the name given in the row with the ID and the
            column name known.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table from where collect the value),
                    row_id (Row of the table from where collect the value),
                    column_name (The name of the column from where collect the value).
            RETURNS: A string with the value, False if something went wrong.

        -> insert_row

            Insert a new row in the table given.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table where add the row),
                    new_row (A list with the complete new row to add to the table).
            RETURNS: True if the row was inserted, False if not.

        -> update_value

            Change a value in a table.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table where change the value),
                    row_id (Row ID where change the value),
                    column_name (The column name where change the value),
                    new_value (The value to put instead of the old one).
            RETURNS: True if value was changed, False if something went wrong.

        -> get_row_id

            Get the ID of the row needed.

            PARAMS: db_path (String with the Data Base path),
                    table_name (Name of the table where the row is),
                    column_name (A known column name to locate the row needed),
                    column_value (The value for the known column to locate the row needed).
            RETURNS: The ID, False if something went wrong.
"""

import sqlite3
import re

def cpath(path):
    """ Check DB path """

    if not isinstance(path, str):
        return False

    if re.search("^[a-zA-Z0-9./]+$", path) is None:
        return False

    return True

def create_table(db_path, table_name, columns_info):
    """ Create a Table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(columns_info, str)
    ):
        return False

    if re.search("^[a-zA-Z0-9_]+$", table_name) is None or not columns_info:
        return False

    for column in columns_info:

        column = columns_info[column]

        if not 'name' in column.keys() or not 'type' in column.keys():
            return False

        if re.search("^[a-zA-Z0-9_]+$", column['name']) is None:
            return False

        if not column['type'] in ('NULL', 'INTEGER', 'REAL', 'TEXT', 'BLOB'):
            return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'CREATE TABLE IF NOT EXISTS ' + table_name + ' ('

    for column in columns_info:

        column = columns_info[column]

        query += column['name'] + ' ' + column['type']

        if 'extra' in column.keys():
            query += query + ' ' + column['extra']

        query += ', '

    query = query[:-2] + ')'

    cursor.execute(query)
    connection.commit()
    connection.close()

    return True

def delete_table(db_path, table_name):
    """ Delete a Table """

    if not isinstance(table_name, str) or not cpath(db_path):
        return False

    if re.search("^[a-zA-Z0-9_]+$", table_name) is None:
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'DROP TABLE ' + table_name
    result = False

    try:

        cursor.execute(query)
        connection.commit()
        result = True

    except sqlite3.OperationalError:

        connection.close()
        result = True

    finally:

        connection.close()

    return result

def select_table(db_path, table_name):
    """ Select a Table """

    if not isinstance(table_name, str) or not cpath(db_path):
        return False

    if re.search("^[a-zA-Z0-9_]+$", table_name) is None:
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'SELECT * FROM ' + table_name
    result = False

    try:

        table = cursor.execute(query)
        result = table.fetchall()

    except sqlite3.OperationalError:

        pass

    finally:

        connection.close()

    return result

def select_row(db_path, table_name, row_id):
    """ Select a Row from a Table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(row_id, int)
    ):
        return False

    if re.search("^[a-zA-Z0-9_]+$", table_name) is None:
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'SELECT * FROM ' + table_name + ' WHERE ROWID=' + str(row_id)
    result = False

    try:

        row = cursor.execute(query)
        result = row.fetchall()

        if not result:
            result = False

    except sqlite3.OperationalError:

        pass

    finally:

        connection.close()

    return result

def select_column(db_path, table_name, column_name):
    """ Select a Column from a Table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(column_name, str)
    ):
        return False

    if (
            re.search("^[a-zA-Z0-9_]+$", table_name) is None or
            re.search("^[a-zA-Z0-9_]+$", column_name) is None
    ):
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'SELECT ' + column_name + ' FROM ' + table_name
    result = False

    try:

        row = cursor.execute(query)
        result = row.fetchall()

    except sqlite3.OperationalError:

        pass

    finally:

        connection.close()

    return result

def select_value(db_path, table_name, row_id, column_name):
    """ Select a Value from a Table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(column_name, str) or
            not isinstance(row_id, int)
    ):
        return False

    if (
            re.search("^[a-zA-Z0-9_]+$", table_name) is None or
            re.search("^[a-zA-Z0-9_]+$", column_name) is None
    ):
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'SELECT ' + column_name + ' FROM ' + table_name + ' WHERE ROWID=' + str(row_id)
    result = False

    try:

        value = cursor.execute(query)
        result = value.fetchall()

        if not result:
            result = False

    except sqlite3.OperationalError:

        pass

    finally:

        connection.close()

    return result

def insert_row(db_path, table_name, new_row):
    """ Insert a Row in a Table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(new_row, list)
    ):
        return False

    if re.search("^[a-zA-Z0-9_]+$", table_name) is None or not new_row:
        return False

    for value in new_row:

        if re.search("^[a-zA-Z0-9_]+$", value) is None:
            return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'INSERT INTO ' + table_name + ' VALUES("'

    for value in new_row:

        query += value + '", "'

    query = query[:-3] + ')'

    cursor.execute(query)
    connection.commit()
    connection.close()

    return True

def update_value(db_path, table_name, row_id, column_name, new_value):
    """ Update a value from row in a Table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(row_id, int) or
            not isinstance(column_name, str) or
            not isinstance(new_value, str)
    ):
        return False

    if (
            re.search("^[a-zA-Z0-9_]+$", table_name) is None or
            re.search("^[a-zA-Z0-9_]+$", column_name) is None or
            re.search("^[a-zA-Z0-9_]+$", new_value) is None
    ):
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'UPDATE ' + table_name
    query += ' SET ' + column_name
    query += '="' + new_value + '"'
    query += ' WHERE ROWID=' + str(row_id)
    result = False

    try:

        query = cursor.execute(query)
        result = True

    except sqlite3.OperationalError:

        pass

    finally:

        connection.close()

    return result

def get_row_id(db_path, table_name, column_name, column_value):
    """ Get Row ID from a row in a table """

    if(
            not isinstance(table_name, str) or
            not cpath(db_path) or
            not isinstance(column_name, str) or
            not isinstance(column_value, str)
    ):
        return False

    if (
            re.search("^[a-zA-Z0-9_]+$", table_name) is None or
            re.search("^[a-zA-Z0-9_]+$", column_name) is None or
            re.search("^[a-zA-Z0-9_]+$", column_value) is None
    ):
        return False

    connection = sqlite3.connect(db_path)
    cursor = connection.cursor()

    query = 'SELECT ROWID FROM ' + table_name + ' WHERE ' + column_name + '="' + column_value + '"'
    result = False

    try:

        value = cursor.execute(query)
        result = value.fetchall()

        if not result:
            result = False

        else:
            result = int(result[0][0])

    except sqlite3.OperationalError:

        pass

    finally:

        connection.close()

    return result
