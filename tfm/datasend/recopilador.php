<?php

	function obtenerUltimoEnvio ( $db ) {

		$num  = $db->query ( 'SELECT COUNT() FROM sends' );
		$num  = $num->fetchArray();
		$num  = $num['COUNT()'];

		$last = $db->query ( 'SELECT * FROM sends ORDER BY unix DESC LIMIT 1' );
		$last = $last->fetchArray();
		$last = $last['unix'];

		return $last;

	}

	function prepararDatos ( $db, $last ) {

		$data   = $db->query ( 'SELECT * FROM records' );
		$datos  = [];
		$i      = 0;

		while ( $row = $data->fetchArray() ) {

			if ( intval($row['unix']) < intval($last) )
				continue;

			$day = date('d,m,Y', $row['unix']);
			$hour = date('H,i,s', $row['unix']);

			$datos[$i]['flavour'] = $row['flavour'];
			$datos[$i]['date'] = $day;
			$datos[$i]['hour'] = $hour;
			$datos[$i]['selected_cuantity'] = $row['selected_cuantity'];

			$i = $i + 1;

		}

		$datos = json_encode ( $datos );

		return $datos;

	}

	# Inicializamos el link con la BD y el archivo configuración
	$db = new SQLite3 ( '/home/pi/database/database.db' );

	# Comprobar Último Envío
	$last = obtenerUltimoEnvio ( $db );

	# Array's con los datos necesarios
	$datos = prepararDatos ( $db, $last );

	$db->close();

	//var_dump($datos);

	return $datos;

?>
