import serial, glob, time, sqlite3, datetime, os

# Get devices conected by USB and established serial
def serial_connect():

	try:
		device = glob.glob('/dev/ttyUSB*')
		conection = serial.Serial(device[0],baudrate=9600)
		return conection

	except:

		return False


# Conect database to write messages
def db_connect():

	try:

		conn = sqlite3.connect('/home/pi/tfm/database/data_storage.db')
		conection = conn.cursor()
		return conn, conection

	except:

		print('Could not be connected to Database')
		return False


# Save tasks function
def save_record(ser, cur, db, data):

	id = data[0]
	state = data[1]
	value = data[2]
	time = str(datetime.datetime.utcnow().timestamp()).split('.')

	cur.execute('INSERT INTO records (device_id , state, value, datetime) VALUES ("{}", "{}", "{}", "{}")'.format(id, state, value, time[0]))
	db.commit()

# Send message to operational module

def check_message():
	new_data = False

	with open('/home/pi/tfm/web_receiver.txt', 'r') as file:
		
		content = file.readline().replace('/n','').split(',')
		if 'no' not in content:
			ser.write('{}|{}'.format(content[0],content[1]).encode('utf-8'))
			new_data = True

	if new_data:

		with open('/home/pi/tfm/web_receiver.txt', 'w') as file:
			file.write('no,no')

ser = False
cur = False

while True:

	try:

		while not ser or not cur:

			if not ser:
				ser = serial_connect()

			if not cur:
				db, cur = db_connect()

			time.sleep(1)

		while ser:

			data = ser.readline().decode('utf-8').strip().split('|')
			check_message()
			#if data[0] == 'save':
			print (data)
			save_record(ser, cur, db, data)

	
	except KeyboardInterrupt:

		if ser:
			ser.close()

		if cur:
			cur.close()

		break

	except:

		ser = False
		continue