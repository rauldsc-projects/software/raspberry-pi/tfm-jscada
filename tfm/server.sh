#!/bin/bash

ip=$( hostname -I | awk '{print $1}' )

cd /var/www/html;
sudo php -S $ip:80
